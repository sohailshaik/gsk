import React from 'react';

function Header() {
  return (
    <section className='header'>
        <div className="container">
            <header className='flex between align__center'>
                
                <div>
                    <h2 className="brand__name">BUDGET DISSEMINATION TOOL</h2>
                </div>
                <div>
                    <img className='user__info__img' src="/Media/user__info.png" alt="" />
                </div>
            </header>
        </div>
    </section>
  )
}

export default Header