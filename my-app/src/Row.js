import React,{useEffect, useState} from 'react'

function Row(props) {
    // console.log(props,"ppppppppppppppppppp")
    const [adjustrci,setAdjustRCI] = useState([])
    let allBetnovateAdjustedBudget = document.querySelectorAll(`#Betnovate__adjusted`)
    let allBetnovateUnAdjustedBudget = document.querySelectorAll(`#Betnovate__unadjusted`)
    let alltbactcreamAdjustedBudget = document.querySelectorAll(`#T-Bact__adjusted`)
    let clubAll = [...allBetnovateAdjustedBudget,...alltbactcreamAdjustedBudget]
    let clubbedAllValues = [];
    let AdjustedRCI = [];
    let unAdjustedBetnovateTotal = 0;

    for(let i = 0 ; i < allBetnovateUnAdjustedBudget.length ; i++) {
        unAdjustedBetnovateTotal += Number(allBetnovateUnAdjustedBudget[i].textContent)
    }


    for(let i=0;i<clubAll.length;i++){
        clubbedAllValues.push(Number(clubAll[i]?.textContent))
    }
    for(let i=0;i<allBetnovateAdjustedBudget.length;i++){
        // console.log(clubbedAllValues,props?.totals?.totalAdjustedBetnovate,"vvvvvvvvvvvvvvvvvvv")
        AdjustedRCI.push((Number(clubbedAllValues[i])/Number(props?.totals?.totalAdjustedBetnovate))*100)
        
    }
    for(let i=allBetnovateAdjustedBudget.length;i<clubbedAllValues.length;i++){
        // console.log(clubbedAllValues[i],"wowwwwwwwwwww")
        // console.log(props?.totals?.totalAdjustedBact,"wowwwwwwwwwwwww")
        AdjustedRCI.push((Number(clubbedAllValues[i])/Number(props?.totals?.totalAdjustedBact))*100)
    }
    // console.log(props?.total?.totalAdjustedBetnovate,"xxxxxxxxxxxxxxxxxxxx")
    // console.log(AdjustedRCI,"outttttttttttttttttttttttttttt")
    useEffect(()=>{
        if(props?.totals?.totalAdjustedBetnovate>0||props?.totals?.totalAdjustedBact>0){
            // console.log(AdjustedRCI,"adjustedddddddddddddRCIIIIIIIIIIIIIIIIIII")
            setAdjustRCI(AdjustedRCI);
        }
    },[props.values])

    // console.log(props?.totals?.totalAdjustedBetnovate, "adjusted budget count")
    // console.log(unAdjustedBetnovateTotal, "un adjusted budget count")
    
    // const diff = Math.abs(props?.totals?.totalAdjustedBetnovate - unAdjustedBetnovateTotal)
    // console.log(diff, "diff between two")

    let betnovate = document.querySelectorAll('.Betnovate')
    let betnovateArray = Array.from(betnovate);
    // console.log(betnovateArray, "array")
    let temp = 0;
    for(let i = 0 ; i < betnovateArray.length ; i++) {
        temp += Number(betnovateArray[i].value)
    }

    let tbact = document.querySelectorAll('.T-Bact')
    let tbactArray = Array.from(tbact);
    // console.log(tbactArray, "array")
    let tbactTotal = 0;
    for(let i = 0 ; i < tbactArray.length ; i++) {
        tbactTotal += Number(tbactArray[i].value)
    }
    
    
  return (
    <>
       {props.data.map((eachRow,i)=>(
        <>
            <tr className="border-b">
                <td className="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {eachRow.RBMCODE}
                </td>
                <td className="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {eachRow.RBMNAME}
                </td>
                <td className="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {eachRow.ABMCODE}
                </td>
                <td className="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {eachRow.ABMNAME}
                </td>
                <td className="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {eachRow.PRODCODE}
                </td>
                <td className="prod__description text-sm text-gray-900 font-light px-0.2 py-2 w-2 whitespace-pre">
                    {eachRow.PRODDESC}
                </td>
                <td className="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {eachRow.BRAND}
                </td>
                <td className="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {(parseFloat(eachRow.Median).toFixed(4))*100}
                </td>
                <td className="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {((eachRow.Max).toFixed(4))*100}
                </td>
                <td id={`${eachRow.BRAND}__unadjusted`} className="Unadjusted__Budget__UN__1 text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {Math.floor(eachRow["BUD Unadjusted"].toFixed(2))}
                </td>
                <td className="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    <input type="text" className={`${eachRow.BRAND}`} name={`data-${i}`} onChange={props.handleChange} value={props.values[`data-${i}`]==null?0:props.values[`data-${i}`]}/>
                    {/* <div>{props?.totals?.totalAdjustedBetnovate>0?"Beyond RBM Budget":""}</div>
                    <div>{props?.totals?.totalAdjustedBetnovate<0?"Below RBM Budget":""}</div> */}
                    {/* <div>{Number(adjustrci[i]) > ((eachRow.Max).toFixed(4))*100 ? "Beyond RBM Budget":""}</div>
                    <div>{Number(adjustrci[i]) < (parseFloat(eachRow.Median).toFixed(4))*100 ? "Below RBM Budget":""}</div>
                    <div>{Number(adjustrci[i]) >= (parseFloat(eachRow.Median).toFixed(4))*100 && Number(adjustrci[i]) <= ((eachRow.Max).toFixed(4))*100 ? "Relevant RBM Budget":""}</div> */}
                    {/* <div>{(Number(props.values[`data-${i}`]==null?0:props.values[`data-${i}`])+Number(eachRow["BUD Unadjusted"])).toFixed(2) > eachRow["BUD Unadjusted"].toFixed(2) ||  Number(props?.totals?.totalAdjustedBetnovate) > Number(unAdjustedBetnovateTotal) ? "Beyond RBM Budget" : (Number(props.values[`data-${i}`]==null?0:props.values[`data-${i}`])+Number(eachRow["BUD Unadjusted"])).toFixed(2) < eachRow["BUD Unadjusted"].toFixed(2) ||  Number(props?.totals?.totalAdjustedBetnovate) <  Number(unAdjustedBetnovateTotal) ? "Below RBM Budget" : (Number(props.values[`data-${i}`]==null?0:props.values[`data-${i}`])+Number(eachRow["BUD Unadjusted"])).toFixed(2) == eachRow["BUD Unadjusted"].toFixed(2) || Number(props?.totals?.totalAdjustedBetnovate) == Number(unAdjustedBetnovateTotal) ? "Relevant RBM Budget" : ""}</div> */}
                    {
                        eachRow.BRAND === "Betnovate" ? <div>{props?.totals?.totalAdjustedBetnovate > unAdjustedBetnovateTotal && temp !==0 ? <h2 className="error__messages">Beyond RBM Budget</h2> : props?.totals?.totalAdjustedBetnovate < unAdjustedBetnovateTotal && temp < 0  ? <h2 className="error__messagess">Below RBM Budget</h2> : props?.totals?.totalAdjustedBetnovate == unAdjustedBetnovateTotal && temp == 0 ? <h2 className="error__messages">Relevant RBM Budget</h2> : ""}</div>
                        :
                        eachRow.BRAND === "T-Bact" ? <div>{props?.totals?.totalAdjustedBetnovate > unAdjustedBetnovateTotal && tbactTotal !==0 ? <h2 className="error__messages">Beyond RBM Budget</h2> : props?.totals?.totalAdjustedBetnovate < unAdjustedBetnovateTotal && tbactTotal < 0  ? <h2 className="error__messagess">Below RBM Budget</h2> : props?.totals?.totalAdjustedBetnovate == unAdjustedBetnovateTotal && tbactTotal == 0 ? <h2 className="error__messages">Relevant RBM Budget</h2> : ""}</div> : ""
                    }
                </td>
                <td id={`${eachRow.BRAND}__adjusted`} className="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {Math.floor((Number(props.values[`data-${i}`]==null?0:props.values[`data-${i}`])+Number(eachRow["BUD Unadjusted"])).toFixed(2))}
                </td>
                <td className="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {parseFloat(adjustrci[i]).toFixed(2) === "NaN" ? "" : parseFloat(adjustrci[i]).toFixed(2)}
                </td>
                <td className="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {/* {(Number(adjustrci[i])<Number(eachRow.Max))&& (Number(adjustrci[i])>Number(eachRow.Median))?"with in limit":"out"} */}
                    <div>{Number(adjustrci[i]) > ((eachRow.Max).toFixed(4))*100 ? "Out Range": Number(adjustrci[i]) < (parseFloat(eachRow.Median).toFixed(4))*100 ? "Below the range" : Number(adjustrci[i]) >= (parseFloat(eachRow.Median).toFixed(4))*100 && Number(adjustrci[i]) <= ((eachRow.Max).toFixed(4))*100 ? "In Range":""}</div>
                </td>
            </tr>
        </>
        ))}
    </>
  )
}

export default Row