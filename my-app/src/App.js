// import './App.css';
import "./new.css"
import React from 'react';
import Dashboard from './Dashboard';
import Header from './Header';
import { NavLink } from 'react-router-dom';

import { BrowserRouter as Router, Route, Switch, withRouter, Redirect } from 'react-router-dom';
import AbmRephq from './AbmRephq';
import GrowthSummary from './GrowthSummary';
import SideBar from './SideBar';
import AbmBudgetSummary from "./AbmBudgetSummary";
import RepBudgetSummary from "./RepBudgetSummary";
import ProxyCp from "./ProxyCp";

function App() {
  return (
    <>
    <div className='flex around'>
      <section className="flex__20">
          <SideBar />
      </section>
      <section className="flex__78">
        <Header />
        <div>

        <Route path="/" exact>
          <Dashboard/>
        </Route>
        <Route path="/abm-rephq" exact>
          <AbmRephq/>
        </Route>
        <Route path="/growth-summary" exact>
          <GrowthSummary/>
        </Route>
        <Route path="/abm-budget-summary" exact>
          <AbmBudgetSummary/>
        </Route>
        <Route path="/rep-budget-summary" exact>
          <RepBudgetSummary/>
        </Route>
        <Route path="/proxy-cp" exact>
          <ProxyCp/>
        </Route>
        </div>

      </section>
    </div>
    </>
  );
}

export default App;
