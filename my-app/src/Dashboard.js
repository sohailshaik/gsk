import React,{useEffect, useState} from 'react'
import data from "./data.json"
import Row from "./Row"
import SideBar from './SideBar';

function Dashboard() {
    const [unadjusted1,setUnadjusted1] = useState(0);
    const [unadjusted2,setUnadjusted2] = useState(0);
    const [unadjusted3,setUnadjusted3] = useState(0);
    const [unadjusted4,setUnadjusted4] = useState(0);
    const [unadjusted1comment,setUnadjusted1comment] = useState("Equal");
    const [unadjusted2comment,setUnadjusted2comment] = useState("Equal");
    const [unadjusted3comment,setUnadjusted3comment] = useState("Equal");
    const [unadjusted4comment,setUnadjusted4comment] = useState("Equal");
    const [select1,setSelect1] = useState("")
    const [select2,setSelect2] = useState("")
    const [select3,setSelect3] = useState("")
    const [select4,setSelect4] = useState("")
    const [values,setValues] = useState({});
    const [totalbetnovatevalue,setTotalBetnovate] = useState(0)
    const [adjustedbetnovatetotal,setTotalAdjustedBetnovate] = useState(0)
    const [totaladjustedbact,setTotalAdjustedBact] = useState(0)
    const [totals,setTotals] = useState({})
    // useEffect(()=>{
    //     setSelect1(document.querySelector(".Unadjusted__Budget__UN__1").textContent)
    //     // setSelect2(document.querySelector(".Unadjusted__Budget__UN__2").textContent)
    //     // setSelect3(document.querySelector(".Unadjusted__Budget__UN__3").textContent)
    //     // setSelect4(document.querySelector(".Unadjusted__Budget__UN__4").textContent)
    // },[])
    // console.log(data,"data")
    // const handleChange = (event) =>{
        
    //     setSelect1(document.querySelector(".Unadjusted__Budget__UN__1").textContent)
    //     // setSelect2(document.querySelector(".Unadjusted__Budget__UN__2").textContent)
    //     // setSelect3(document.querySelector(".Unadjusted__Budget__UN__3").textContent)
    //     // setSelect4(document.querySelector(".Unadjusted__Budget__UN__4").textContent)

    //     console.log(event)
    //     if(event.target.classNameList.contains("one")){
    //         setUnadjusted1(event.target.value)
    //     }
    //     if(event.target.classNameList.contains("two")){
    //         setUnadjusted2(event.target.value)
    //     }
    //     if(event.target.classNameList.contains("three")){
    //         setUnadjusted3(event.target.value)
    //     }
    //     if(event.target.classNameList.contains("four")){
    //         setUnadjusted4(event.target.value)
    //     }        
    // }
    
    useEffect(()=>{
        let totalBetnovate = 0
        let totalAdjustedBetnovate = 0
        let totalAdjustedBact = 0
        const some  = document.querySelectorAll(".Betnovate")
        let adjustedBetnovate = document.querySelectorAll("#Betnovate__adjusted")
        // console.log(some,"someeeeeee")
        
        let adjusted_T_Bact_adjusted = document.querySelectorAll("#T-Bact__adjusted")
        for(let i=0;i<some.length;i++){
            // console.log("went inside")
            totalBetnovate+=Number(some[i].value)
            totalAdjustedBetnovate+=Number(adjustedBetnovate[i].textContent)
            totalAdjustedBact+=Number(adjusted_T_Bact_adjusted[i].textContent)
        }
        if(totalBetnovate!=0){
            setTotals({...totals,"totalBetnovate":totalBetnovate,"totalAdjustedBetnovate":totalAdjustedBetnovate,"totalAdjustedBact":totalAdjustedBact})
        }
        
    },[values])
    // setTotalAdjustedBetnovate(totalAdjustedBetnovate)
    // setTotalAdjustedBact(totalAdjustedBact)
    const handleChange = (e) => {    
        const {name,value} = e.target;
        // console.log(name,value)
            setValues({...values,
                [name]:value
            })
    }

    
  return (
    <>
        <section className='flex between'>
            <section>
                {/* <SideBar /> */}
            </section>

            <section className="table__section">
                <div className="container">
                    <div className="">
                        <section className='flex'>
                        <div className="select">
                            <select id=""  className="select__tag">
                                <option value="">Select Month </option>
                                <option value="">option1</option>
                                <option value="">option1</option>
                            </select>
                        </div>

                        <div className="select select__left">
                            <select id=""  className="select__tag">
                                <option value="">Select Brand</option>
                                <option value="">option1</option>
                                <option value="">option1</option>
                            </select>
                        </div>

                        <div className="select select__left">
                            <select id=""  className="select__tag">
                                <option value="">Select Prod Desc</option>
                                <option value="">option1</option>
                                <option value="">option1</option>
                            </select>
                        </div>

                        <div className="select select__left">
                            <select id=""  className="select__tag">
                                <option value="">Select ABM</option>
                                <option value="">option1</option>
                                <option value="">option1</option>
                            </select>
                        </div>

                        <div className="select select__left">
                            <select id=""  className="select__tag">
                                <option value="">Select REPHQ</option>
                                <option value="">option1</option>
                                <option value="">option1</option>
                            </select>
                        </div>
                        </section>
                        <div className="table__bottom">
                            <table className="table">
                                <thead className="">
                                    <tr>
                                        <th scope="col" className="">
                                            ABMCODE
                                        </th>
                                        <th scope="col" className="">
                                            ABMNAME
                                        </th>
                                        <th scope="col" className="">
                                            REPCODE
                                        </th>
                                        <th scope="col" className="">
                                            REPHQ
                                        </th>
                                        <th scope="col" className="">
                                            PRODCODE
                                        </th>
                                        <th scope="col" className="">
                                            PRODDESC
                                        </th>
                                        <th scope="col" className="">
                                            BRAND
                                        </th>
                                        <th scope="col" className="">
                                            MIN
                                        </th>
                                        <th scope="col" className="">
                                            MAX
                                        </th>
                                        <th scope="col" className="">
                                            Unadjusted Budget UN
                                        </th>
                                        <th scope="col" className="">
                                            Delta Adjustment
                                        </th>
                                        <th scope="col" className="">
                                            Adjusted Budget UN
                                        </th>
                                        <th scope="col" className="">
                                            Adjusted RCI
                                        </th>
                                        <th scope="col" className="">
                                            Comments
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <Row totals={totals} totaladjustedbact={totaladjustedbact} adjustedbetnovatetotal={adjustedbetnovatetotal} totalbetadinvalue={totalbetnovatevalue} data={data} handleChange = {handleChange} values={values} unadjusted1={unadjusted1} select1={select1}/>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </>
  )
}

export default Dashboard