import React from 'react';
import { NavLink } from 'react-router-dom';


function SideBar() {
  return (
    <section className=''>
        <div className="sidebar ">
            <div>
                <img className='brand__logo' src="/Media/gsk-logo.jpg" alt="Logo" />
            </div>
           <div className='sidebar1'>
               <h2 className='sidebar__headings'>Budget Dissemination</h2>
               <article>
                   <NavLink to="/" className='navlink' activeClassName='active' exact>
                       RBM
                   </NavLink>
                   <NavLink to="/abm-rephq" className='navlink' activeClassName='active' exact>
                       ABM & REPHQ
                   </NavLink>
               </article>
           </div>
           <div className='sidebar1'>
               <h2 className='sidebar__headings'>Report Pages</h2>
               <article>
                   <NavLink to="/growth-summary" className='navlink'  activeClassName='active' exact>
                       Growth Summary
                   </NavLink>
                   <NavLink to="/abm-budget-summary" className='navlink' activeClassName='active' exact>
                       ABM Budget Summary
                   </NavLink>
                   <NavLink to="/rep-budget-summary" className='navlink' activeClassName='active' exact>
                       REP Budget Summary
                   </NavLink>
                   <NavLink to="/proxy-cp" className='navlink' activeClassName='active' exact>
                       Proxy & C.P.
                   </NavLink>
               </article>
           </div>
        </div>
    </section>
  )
}

export default SideBar